#include <stdio.h>
#include <stdlib.h>

char read_lines(FILE *fp, int n){
    char ch;
    char stop_symbol = *"\n";
    if (n == 0){
        stop_symbol = EOF;
    }
    ch = fgetc(fp);
    while (ch != stop_symbol && ch != EOF){
        printf("%c", ch);
        ch = fgetc(fp);
    }
    printf("\n");
    return ch;
}


int main(int argc, char *argv[]) {
    int n = (int) *argv[2] - 48;
    FILE *fp;

    fp = fopen(argv[1], "r");

    if (fp == NULL) {
        printf("Cannot open file.\n");
        exit (1);
    }

    if (n == 0){
        read_lines(fp, 0);
    }
    else {
        char ch;
        while (1) {
            for (int i = 0; i < n; ++i) {
                ch = read_lines(fp, 1);
                if (ch == EOF){
                    fclose(fp);
                    return 0;
                }
            }
            getc(stdin);

        }
    }

    fclose(fp);
    return 0;
}
