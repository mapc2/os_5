#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>
#include <libgen.h>


char ** all_files;
int n = 0;
int iter = 0;


long int find_size(const char* file_name)
{
    struct stat st;

    if (stat(file_name, &st) == 0)
        return (st.st_size);
    else
        return -1;
}


int comp_file_name(const void* a, const void* b)
{
    return strcmp(basename(*(char**)a), basename(*(char**)b));
}

int comp_file_size(const void* a, const void* b){
    if (find_size(*(const char**)a) > find_size(*(const char**)b)) return 1;
    if (find_size(*(const char**)a) < find_size(*(const char**)b)) return -1;
    return 0;
}


void process_dir(char dir_name[])
{
    DIR *dir;
    dir = opendir(dir_name);

    rewinddir(dir);
    struct dirent *obj;
    obj = readdir(dir);

    while(obj != NULL) {
        if (obj->d_type == DT_REG) {
            if (iter >= n){
                n += 10;
                all_files = realloc(all_files, n * 256 * sizeof(char));
            }
            char* fullpath = (char*)malloc(255);
            fullpath[0] = '\0';
            fullpath = strncat(fullpath, dir_name, strlen(dir_name));
            fullpath = strncat(fullpath, "/", 1);
            fullpath = strncat(fullpath, obj->d_name, strlen(obj->d_name));
            all_files[iter] = fullpath;
            iter ++;
        }
        else {
            if (obj->d_type == DT_DIR && strcmp(obj->d_name, "...") != 0 && strcmp(obj->d_name, "..") != 0 && strcmp(obj->d_name, ".") != 0) {
                char* fullpath = (char*)malloc(255);
                fullpath[0] = '\0';
                fullpath = strncat(fullpath, dir_name, strlen(dir_name));
                fullpath = strncat(fullpath, "/", 1);
                fullpath = strncat(fullpath, obj->d_name, strlen(obj->d_name));
                process_dir(fullpath);
                free(fullpath);
            }
        }
        obj = readdir(dir);
    }

    closedir(dir);
}


int main(int argc, char *argv[])
{
    n = 10;
    all_files = calloc(n, sizeof(char *));

    process_dir(argv[1]);

    if (strcmp(argv[2], "1") == 0){
        qsort(all_files, iter, sizeof(const char*), comp_file_size);
    }
    else{
        qsort(all_files, iter, sizeof(const char*), comp_file_name);
    }

    for (int i = 0; i < iter; ++i) {
        printf("%s\t%ld\n", all_files[i], find_size(all_files[i]));
    }

    free(all_files);
    return 0;
}