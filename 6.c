#include <stdio.h>
#include <unistd.h>
#include <dirent.h>

void run(char dir_name[]){
    DIR *dir;
    struct dirent *entry;

    printf("Directory: %s\n", dir_name);

    dir = opendir(dir_name);
    if(dir == NULL)
    {
        printf("Unable to read directory %s", dir_name);
    }
    else{
        entry = readdir(dir);
        while(entry != NULL)
        {
            printf("%s\n", entry->d_name);
            entry = readdir(dir);
        }
        closedir(dir);
    }
}

int main(int argc, char *argv[]) {
    char curr_dir[256];
    getcwd(curr_dir, sizeof (curr_dir));

    run(argv[1]);

    run(curr_dir);

    return 0;
}
