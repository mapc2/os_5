#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
    FILE *fp;
    fp = fopen(argv[1], "w");

    if (fp == NULL) {
        printf("Cannot open file.\n");
        exit(1);
    }

    char ch;

    ch = fgetc(stdin);
    while(!feof(stdin)) {
        fputc(ch, fp);
        ch = fgetc(stdin);
    }
    fclose(fp);
    return 0;
}
