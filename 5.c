#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>



int main(int argc, char *argv[]) {
    FILE *fp1, *fp2;
    fp1 = fopen(argv[1], "r");
    fp2 = fopen(argv[2], "w");

    if (fp1 == NULL || fp2 == NULL) {
        printf("Cannot open file.\n");
        exit (1);
    }

    char ch = fgetc(fp1);
    while (ch != EOF) {
        fputc(ch, fp2);
        ch = fgetc(fp1);
    }


    struct stat st;
    stat(argv[1], &st);
    chmod(argv[2], st.st_mode);

    fclose(fp1);
    fclose(fp2);
    return 0;
}
